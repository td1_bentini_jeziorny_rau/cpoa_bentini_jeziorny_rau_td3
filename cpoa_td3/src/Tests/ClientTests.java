package Tests;

import static org.junit.Assert.*;
import normalisation.normalisation;

import org.junit.Test;

public class ClientTests {
	
	@Test
	public void Norm_pays(){
		assertEquals("Luxembourg", normalisation.Norm_pays("letzebuerg"));
		assertEquals("Suisse", normalisation.Norm_pays("schweiz"));
		assertEquals("Luxembourg", normalisation.Norm_pays("Letzebuerg"));
		assertEquals("Belgique", normalisation.Norm_pays("belgium"));
		assertEquals("Belgique", normalisation.Norm_pays("Belgium"));
		assertEquals("Suisse", normalisation.Norm_pays("Switzerland"));
		assertEquals("Suisse", normalisation.Norm_pays("Schweiz"));
		assertEquals("Suisse", normalisation.Norm_pays("switzerland"));
	}

	@Test
	public void Norm_cp(){
		assertEquals("5515", normalisation.Norm_cp("L-5515"));
		assertEquals("05515", normalisation.Norm_cp("5515"));
	}
	
	@Test
	public void Norm_voie(){
		assertEquals("3, boulevard des alouettes", normalisation.Norm_Voie("3 boul des alouettes"));
		assertEquals("15, place des alouettes", normalisation.Norm_Voie("15 pl. des alouettes"));
	}
	
	@Test
	public void Norm_ville(){
		assertEquals("Sainte-Julien-L�s-Metz", normalisation.Norm_ville("ste julien l�s metz"));
		assertEquals("Moulin-L�s-Metz", normalisation.Norm_ville("moulin l�s metz"));
	}
	
}
