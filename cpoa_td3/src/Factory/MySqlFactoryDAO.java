package Factory;

import Dao.*;
import Fonction.*;

public class MySqlFactoryDAO extends FactoryDAO{

	@Override
	public ClientDAO getClientDAO() {
		return ClientSQL.getInstance();
	}

	@Override
	public AbonnementDAO getAbonnementDAO() {
		return AboSQL.getInstance();
	}
	
	@Override
	public DureeDAO getDureeDAO() {
		return DureeSQL.getInstance();
	}
	
	@Override
	public FormuleDAO getFormuleDAO() {
		return FormuleSQL.getInstance();
	}
	
	@Override
	public PeriodiciteDAO getPeriodiciteDAO() {
		return PerioSQL.getInstance();
	}
	
	@Override
	public RevueDAO getRevueDAO() {
		return RevueSQL.getInstance();
	}
}
