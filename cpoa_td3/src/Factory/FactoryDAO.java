package Factory;

import Dao.*;
import enumerations.Persistence;

public abstract class FactoryDAO {
	
		public static FactoryDAO getDAOFactory(Persistence cible) {
			FactoryDAO daoF = null;
			switch (cible) {
			case MYSQL:
				daoF = new MySqlFactoryDAO();
				break;
			case ListeMemoire:
				daoF = new ListeMemoireFactoryDAO();
				break;
			default:
				break;
			}
			return daoF;
		}

		public abstract ClientDAO getClientDAO();
		public abstract AbonnementDAO getAbonnementDAO();
		public abstract DureeDAO getDureeDAO();
		public abstract FormuleDAO getFormuleDAO();
		public abstract PeriodiciteDAO getPeriodiciteDAO();
		public abstract RevueDAO getRevueDAO();
		

}
