package Factory;

import ListeMemoire.*;


public class ListeMemoireFactoryDAO extends FactoryDAO{

	@Override
	public ListeMemoireClientDAO getClientDAO() {
		return ListeMemoireClientDAO.getInstance();
	}

	@Override
	public ListeMemoireAbonnementDAO getAbonnementDAO() {
		return ListeMemoireAbonnementDAO.getInstance();
	}
	
	@Override
	public ListeMemoireDureeDAO getDureeDAO() {
		return ListeMemoireDureeDAO.getInstance();
	}
	
	@Override
	public ListeMemoireFormuleDAO getFormuleDAO() {
		return ListeMemoireFormuleDAO.getInstance();
	}
	
	@Override
	public ListeMemoirePeriodiciteDAO getPeriodiciteDAO() {
		return ListeMemoirePeriodiciteDAO.getInstance();
	}
	
	@Override
	public ListeMemoireRevueDAO getRevueDAO() {
		return ListeMemoireRevueDAO.getInstance();
	}
}
