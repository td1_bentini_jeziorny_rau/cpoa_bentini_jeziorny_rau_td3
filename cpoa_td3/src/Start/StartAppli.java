package Start;

import java.util.Scanner;

import ObjetM�tier.Abonnement;
import ObjetM�tier.Client;
import ObjetM�tier.Duree;
import ObjetM�tier.Formule;
import ObjetM�tier.Periodicite;
import ObjetM�tier.Revue;
import Factory.FactoryDAO;
import enumerations.*;

public class StartAppli {

	public static Scanner sc;

	public static void main(String[] args) {
		sc = new Scanner(System.in);
		FactoryDAO daos = null;
		
		System.out.println("Veuillez saisair la methode a utiliser : (mysql, liste_memoire)");
		String mode = sc.nextLine();
		switch(mode){
		case "mysql":
			daos = FactoryDAO.getDAOFactory(Persistence.MYSQL);
			break;
		case "liste_memoire":	
			daos = FactoryDAO.getDAOFactory(Persistence.ListeMemoire);
			break;
		default:
			System.out.println("Methode inconnue veuillez r�essayer");
			return;
		}
		
		
		
		

		System.out.println(
				"Veuillez saisir la table � traiter (abonnement, client, duree, formule, periodicite, revue) :");
		String table = sc.nextLine();

		switch (table) {
		case "periodicite":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String perio = sc.nextLine();

			switch (perio) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id:");
				String id = sc.nextLine();
				int id2 = Integer.parseInt(id);
				System.out.println("Veuillez saisir le nouveau libelle:");
				String lib = sc.nextLine();
				Periodicite p=new Periodicite(id2, lib);
				daos.getPeriodiciteDAO().create(p);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id de la ligne � supprimer:");
				String id_sup = sc.nextLine();
				int id_sup2 = Integer.parseInt(id_sup);
				Periodicite p1=new Periodicite(id_sup2);
				daos.getPeriodiciteDAO().delete(p1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id a modifier:");
				String id_mod = sc.nextLine();
				int id_mod2 = Integer.parseInt(id_mod);
				System.out.println("Veuillez saisir le nouveau libelle:");
				String lib_mod = sc.nextLine();
				Periodicite p2=new Periodicite(id_mod2, lib_mod);
				daos.getPeriodiciteDAO().update(p2);
				break;
			}
			break;

		case "revue":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String revue = sc.nextLine();

			switch (revue) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id_revue:");
				int idr_add = sc.nextInt();
				System.out.println("Veuillez saisir le nouveau titre:");
				String titre = sc.next();
				System.out.println("Veuillez saisir la nouvelle description:");
				String desc = sc.next();
				System.out.println("Veuillez saisir le nouveau tarif:");
				String tarif = sc.next();
				float tarif2 = Float.parseFloat(tarif);
				System.out.println("Veuillez saisir le nouveau visuel:");
				String visu = sc.next();
				System.out.println("Veuillez saisir le nouvel id_perio:");
				int idp = sc.nextInt();
				Revue r=new Revue(idr_add, titre, desc, tarif2, visu, idp);
				daos.getRevueDAO().create(r);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id de la ligne � supprimer:");
				String id_sup = sc.nextLine();
				int id_sup2 = Integer.parseInt(id_sup);
				Revue r1=new Revue(id_sup2);
				daos.getRevueDAO().delete(r1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id_revue a modifier:");
				String id_mod = sc.nextLine();
				int id_mod2 = Integer.parseInt(id_mod);
				System.out.println("Veuillez saisir le nouveau titre:");
				String titre_mod = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle description:");
				String desc_mod = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau tarif:");
				String tarif_mod = sc.nextLine();
				int tarif_mod2 = Integer.parseInt(tarif_mod);
				System.out.println("Veuillez saisir le nouveau visuel:");
				String visu_mod = sc.nextLine();
				System.out.println("Veuillez saisir le nouvel id_perio:");
				int idp_mod = sc.nextInt();
				Revue r2=new Revue(id_mod2, titre_mod, desc_mod, tarif_mod2, visu_mod, idp_mod);
				daos.getRevueDAO().update(r2);
				break;
			}
			break;

		case "client":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String cli = sc.nextLine();

			switch (cli) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id:");
				String id_cli = sc.nextLine();
				int id_cli2 = Integer.parseInt(id_cli);
				System.out.println("Veuillez saisir le nouveau nom:");
				String nom = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau prenom:");
				String prenom = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau no_rue:");
				String no_rue = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle voie:");
				String voie = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau code postal:");
				String cp = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle ville:");
				String ville = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau pays:");
				String pays = sc.nextLine();
				Client c=new Client(id_cli2, nom, prenom, no_rue, voie, cp, ville, pays);
				daos.getClientDAO().create(c);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id de la ligne � supprimer:");
				String id_sup = sc.nextLine();
				int id_sup2 = Integer.parseInt(id_sup);
				Client c1=new Client(id_sup2);
				daos.getClientDAO().delete(c1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id client a modifier:");
				id_cli = sc.nextLine();
				id_cli2 = Integer.parseInt(id_cli);
				System.out.println("Veuillez saisir le nouveau nom:");
				nom = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau prenom:");
				prenom = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau no_rue:");
				no_rue = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle voie:");
				voie = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau code postal:");
				cp = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle ville:");
				ville = sc.nextLine();
				System.out.println("Veuillez saisir le nouveau pays:");
				pays = sc.nextLine();
				Client c2=new Client(id_cli2, nom, prenom, no_rue, voie, cp, ville, pays);
				daos.getClientDAO().update(c2);
				break;
			}
			break;

		case "abonnement":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String abo = sc.nextLine();

			switch (abo) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id_client:");
				String id_client = sc.nextLine();
				int id_client2 = Integer.parseInt(id_client);
				System.out.println("Veuillez saisir le nouvel id_revue:");
				String id_revue = sc.nextLine();
				int id_revue2 = Integer.parseInt(id_revue);
				System.out.println("Veuillez saisir la nouvelle date de d�but (aaaa-mm-dd):");
				String dd = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle date de fin (aaaa-mm-dd):");
				String df = sc.nextLine();
				System.out.println("Veuillez saisir le nouvel id_duree:");
				String id_dur = sc.nextLine();
				int id_dur2 = Integer.parseInt(id_dur);
				Abonnement a=new Abonnement(id_client2, id_revue2, dd, df, id_dur2);
				daos.getAbonnementDAO().create(a);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id_client de la ligne � supprimer:");
				String id_sup_abo = sc.nextLine();
				int id_sup_abo2 = Integer.parseInt(id_sup_abo);
				System.out.println("Veuillez saisir l'id_revue de la ligne � supprimer:");
				String id_rev_abo = sc.nextLine();
				int id_rev_abo2 = Integer.parseInt(id_rev_abo);
				Abonnement a1=new Abonnement(id_sup_abo2, id_rev_abo2);
				daos.getAbonnementDAO().delete(a1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id_client a modifier:");
				String id_client_mod = sc.nextLine();
				int id_client_mod2 = Integer.parseInt(id_client_mod);
				System.out.println("Veuillez saisir l'id_revue a modifier:");
				String id_revue_mod = sc.nextLine();
				int id_revue_mod2 = Integer.parseInt(id_revue_mod);
				System.out.println("Veuillez saisir la nouvelle date de d�but (aaaa-mm-dd):");
				String dd_mod = sc.nextLine();
				System.out.println("Veuillez saisir la nouvelle date de fin (aaaa-mm-dd):");
				String df_mod = sc.nextLine();
				System.out.println("Veuillez saisir le nouvel id_duree:");
				String id_dur_mod = sc.nextLine();
				int id_dur_mod2 = Integer.parseInt(id_dur_mod);
				Abonnement a2=new Abonnement(id_client_mod2, id_revue_mod2, dd_mod, df_mod, id_dur_mod2);
				daos.getAbonnementDAO().update(a2);
				break;
			}
			break;

		case "formule":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String form = sc.nextLine();

			switch (form) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id_revue:");
				String id_revue_abo = sc.nextLine();
				int id_revue_abo2 = Integer.parseInt(id_revue_abo);
				System.out.println("Veuillez saisir le nouvel id_duree:");
				String id_duree_abo = sc.nextLine();
				int id_duree_abo2 = Integer.parseInt(id_duree_abo);
				System.out.println("Veuillez saisir la nouvelle reduction:");
				String reduc = sc.nextLine();
				float reduc2 = Float.parseFloat(reduc);
				Formule f=new Formule(id_revue_abo2, id_duree_abo2, reduc2);
				daos.getFormuleDAO().create(f);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id_revue de la ligne � supprimer:");
				String id_revue_abo_sup = sc.nextLine();
				int id_revue_abo_sup2 = Integer.parseInt(id_revue_abo_sup);
				System.out.println("Veuillez saisir le nouvel id_duree:");
				String id_duree_abo_sup = sc.nextLine();
				int id_duree_abo_sup2 = Integer.parseInt(id_duree_abo_sup);
				Formule f1=new Formule(id_revue_abo_sup2, id_duree_abo_sup2);
				daos.getFormuleDAO().delete(f1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id_revue a modifier:");
				String id_revue_abo_mod = sc.nextLine();
				int id_revue_abo_mod2 = Integer.parseInt(id_revue_abo_mod);
				System.out.println("Veuillez saisir le nouvel id_duree:");
				String id_duree_abo_mod = sc.nextLine();
				int id_duree_abo_mod2 = Integer.parseInt(id_duree_abo_mod);
				System.out.println("Veuillez saisir la nouvelle reduction:");
				String reduc_mod = sc.nextLine();
				float reducMod2 = Float.parseFloat(reduc_mod);
				Formule f2=new Formule(id_revue_abo_mod2, id_duree_abo_mod2, reducMod2);
				daos.getFormuleDAO().update(f2);
				break;
			}
			break;

		case "duree":
			System.out.println("Veuillez saisir la fonction a utiliser (ajouter,supprimer,modifier) :");
			String dur = sc.nextLine();

			switch (dur) {
			case "ajouter":
				System.out.println("Veuillez saisir le nouvel id:");
				String id_dur = sc.nextLine();
				int id_dur2 = Integer.parseInt(id_dur);
				System.out.println("Veuillez saisir le nouveau libelle:");
				String lib_dur = sc.nextLine();
				Duree d=new Duree(id_dur2, lib_dur);
				daos.getDureeDAO().create(d);
				break;

			case "supprimer":
				System.out.println("Veuillez saisir l'id de la ligne � supprimer:");
				String id_dur_sup = sc.nextLine();
				int id_dur_sup2 = Integer.parseInt(id_dur_sup);
				Duree d1=new Duree(id_dur_sup2);
				daos.getDureeDAO().delete(d1);
				break;

			case "modifier":
				System.out.println("Veuillez saisir l'id_duree a modifier:");
				String id_dur_mod = sc.nextLine();
				int id_dur_mod2 = Integer.parseInt(id_dur_mod);
				System.out.println("Veuillez saisir le nouveau libelle:");
				String lib_dur_mod = sc.nextLine();
				Duree d2=new Duree(id_dur_mod2, lib_dur_mod);
				daos.getDureeDAO().update(d2);
				break;
			}
			break;

		default:
			System.out.println("Table inconnue veuillez r�essayer");
			return;
		}
	}
}
