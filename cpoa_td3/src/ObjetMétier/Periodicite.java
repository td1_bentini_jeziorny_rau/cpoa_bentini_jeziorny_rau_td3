package ObjetMétier;

public class Periodicite {
	private int id;
	private String libelle;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		if (id < 0) {
			throw new IllegalArgumentException("Id de la périodicité non valide !");
		}
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		if (libelle == null || libelle.trim().length() == 0) {
			throw new IllegalArgumentException("Libelle de la périodicité vide !");
		}
		this.libelle = libelle.trim();
	}

	public Periodicite(int id, String libelle) {
		this.setId(id);
		this.setLibelle(libelle);
	}

	public Periodicite(String libelle) {
		this.setId(-1);
		this.setLibelle(libelle);
	}
	
	public Periodicite() {
		this.setId(-1);
	}
	
	public Periodicite(int id) {
		this.setId(id);
	}
}
