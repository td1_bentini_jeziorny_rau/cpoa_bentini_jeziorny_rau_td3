package ObjetM�tier;

public class Abonnement {
	private int idClient;
	private int idRevue;
	private String dateDeb;
	private String dateFin;
	private int idDur;

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		if (idClient < 0) {
			throw new IllegalArgumentException("Id du client non valide !");
		}
		this.idClient = idClient;
	}

	public int getIdRevue() {
		return idRevue;
	}

	public void setIdRevue(int idRevue) {
		if (idRevue < 0) {
			throw new IllegalArgumentException("Id de la revue non valide !");
		}
		this.idRevue = idRevue;
	}

	public String getDateDeb() {
		return dateDeb;
	}

	public void setDateDeb(String dateDeb) {
		this.dateDeb = dateDeb.trim();
	}

	public String getDateFin() {
		return dateFin;
	}

	public void setDateFin(String dateFin) {
		this.dateFin = dateFin.trim();
	}

	public int getIdDur() {
		return idDur;
	}

	public void setIdDur(int idDur) {
		if (idDur < 0) {
			throw new IllegalArgumentException("Id de la dur�e non valide !");
		}
		this.idDur = idDur;
	}

	public Abonnement(int idClient, int idRevue, String dateDeb, String dateFin, int idDur) {
		this.setIdClient(idClient);
		this.setIdRevue(idRevue);
		this.setDateDeb(dateDeb);
		this.setDateFin(dateFin);
		this.setIdDur(idDur);
	}

	public Abonnement(String dateDeb, String dateFin) {
		this.setIdClient(-1);
		this.setIdRevue(-1);
		this.setDateDeb(dateDeb);
		this.setDateFin(dateFin);
		this.setIdDur(-1);
	}
	
	public Abonnement() {
		this.setIdClient(-1);
		this.setIdRevue(-1);
	}
	
	public Abonnement(int idClient,int idRevue) {
		this.setIdClient(idClient);
		this.setIdRevue(idRevue);
	}
}

