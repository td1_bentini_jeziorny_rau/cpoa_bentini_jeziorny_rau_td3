package ObjetM�tier;

public class Formule {
	private int idRev;
	private int idDur;
	private float reduc;

	public int getIdRev() {
		return idRev;
	}

	public void setIdRev(int idRev) {
		if (idRev < 0) {
			throw new IllegalArgumentException("Id de la revue non valide !");
		}
		this.idRev = idRev;
	}

	public int getIdDur() {
		return idDur;
	}

	public void setIdDur(int idDur) {
		if (idDur < 0) {
			throw new IllegalArgumentException("Id de la dur�e non valide !");
		}
		this.idDur = idDur;
	}

	public float getReduc() {
		return reduc;
	}

	public void setReduc(float reduc) {
		if (reduc < 0) {
			throw new IllegalArgumentException("R�duction non valide !");
		}
		this.reduc = reduc;
	}

	public Formule(int idRev, int idDur, float reduc) {
		this.setIdRev(idRev);
		this.setIdDur(idDur);
		this.setReduc(reduc);
	}

	public Formule(int idRev, int idDur) {
		this.setIdRev(idRev);
		this.setIdDur(idDur);
	}
	
	public Formule(float reduc) {
		this.setIdRev(-1);
		this.setIdDur(-1);
		this.setReduc(reduc);
	}
	
	public Formule() {
		this.setIdRev(-1);
		this.setIdDur(-1);
	}
}
