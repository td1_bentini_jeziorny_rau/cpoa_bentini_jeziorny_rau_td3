package ObjetMétier;

public class Revue {
	private int idRev;
	private String titre;
	private String desc;
	private float tarif;
	private String visuel;
	private int idPerio;

	public int getIdRev() {
		return idRev;
	}

	public void setIdRev(int idRev) {
		if (idRev < 0) {
			throw new IllegalArgumentException("Id de la revue non valide !");
		}
		this.idRev = idRev;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		if (titre == null || titre.trim().length() == 0) {
			throw new IllegalArgumentException("Titre de la revue vide !");
		}
		this.titre = titre.trim();
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		if (desc == null || desc.trim().length() == 0) {
			throw new IllegalArgumentException("Description de la revue vide !");
		}
		this.desc = desc.trim();
	}

	public float getTarif() {
		return tarif;
	}

	public void setTarif(float tarif) {
		if (tarif < 0) {
			throw new IllegalArgumentException("Tarif non valide !");
		}
		this.tarif = tarif;
	}

	public String getVisuel() {
		return visuel;
	}

	public void setVisuel(String visuel) {
		if (visuel == null || visuel.trim().length() == 0) {
			throw new IllegalArgumentException("Visuel de la revue vide !");
		}
		this.visuel = visuel.trim();
	}

	public int getIdPerio() {
		return idPerio;
	}

	public void setIdPerio(int idPerio) {
		if (idPerio < 0) {
			throw new IllegalArgumentException("Id de la périodicité non valide !");
		}
		this.idPerio = idPerio;
	}
	
	public Revue(int idRev,String titre,String desc,float tarif,String visuel,int idPerio) {
		this.setIdRev(idRev);
		this.setTitre(titre);
		this.setDesc(desc);
		this.setTarif(tarif);
		this.setVisuel(visuel);
		this.setIdPerio(idPerio);
	}
	
	public Revue() {
		this.setIdRev(-1);
	}
	
	public Revue(int idRev) {
		this.setIdRev(idRev);
	}
}
