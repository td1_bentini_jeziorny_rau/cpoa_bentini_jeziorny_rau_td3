package ObjetM�tier;

public class Duree {
	private int id;
	private String libelle;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		if (id < 0) {
			throw new IllegalArgumentException("Id de la dur�e non valide !");
		}
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		if (libelle == null || libelle.trim().length() == 0) {
			throw new IllegalArgumentException("Libelle de la dur�e vide !");
		}
		this.libelle = libelle.trim();
	}

	public Duree(int id, String libelle) {
		this.setId(id);
		this.setLibelle(libelle);
	}

	public Duree(String libelle) {
		this.setId(-1);
		this.setLibelle(libelle);
	}
	
	public Duree() {
		this.setId(-1);
	}
	
	public Duree(int id) {
		this.setId(id);
	}
}
