package ObjetMétier;

public class Client {
	private int idClient;
	private String nom;
	private String prenom;
	private String rue;
	private String voie;
	private String cp;
	private String ville;
	private String pays;

	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int idClient) {
		if (idClient < 0) {
			throw new IllegalArgumentException("Id du client non valide !");
		}
		this.idClient = idClient;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		if (nom == null || nom.trim().length() == 0) {
			throw new IllegalArgumentException("Nom du client vide !");
		}
		this.nom = nom.trim();
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		if (prenom == null || prenom.trim().length() == 0) {
			throw new IllegalArgumentException("Prenom du client vide !");
		}
		this.prenom = prenom.trim();
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		if (rue == null || rue.trim().length() == 0) {
			throw new IllegalArgumentException("Rue du client vide !");
		}
		this.rue = rue.trim();
	}

	public String getVoie() {
		return voie;
	}

	public void setVoie(String voie) {
		if (voie == null || voie.trim().length() == 0) {
			throw new IllegalArgumentException("Voie du client vide !");
		}
		this.voie = voie.trim();
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		if (cp == null || cp.trim().length() == 0) {
			throw new IllegalArgumentException("Code postal du client vide !");
		}
		this.cp = cp.trim();
	}

	public String getPays() {
		return pays;
	}

	public void setPays(String pays) {
		if (pays == null || pays.trim().length() == 0) {
			throw new IllegalArgumentException("Pays du client vide !");
		}
		this.pays = pays.trim();
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		if (ville == null || ville.trim().length() == 0) {
			throw new IllegalArgumentException("Ville du client vide !");
		}
		this.ville = ville.trim();
	}

	public Client(int idClient, String nom, String prenom, String rue, String voie, String cp, String ville,
			String pays) {
		this.setIdClient(idClient);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setRue(rue);
		this.setVoie(voie);
		this.setCp(cp);
		this.setVille(ville);
		this.setPays(pays);
	}

	public Client(String nom, String prenom, String rue, String voie, String cp, String ville, String pays) {
		this.setIdClient(-1);
		this.setNom(nom);
		this.setPrenom(prenom);
		this.setRue(rue);
		this.setVoie(voie);
		this.setCp(cp);
		this.setVille(ville);
		this.setPays(pays);
	}

	public Client(int idClient) {
		this.setIdClient(idClient);

	}

	public Client() {
		this.setIdClient(-1);
	}

	

}
