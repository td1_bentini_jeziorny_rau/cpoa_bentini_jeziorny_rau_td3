package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Connexion.Connexion;
import Dao.ClientDAO;
import ObjetM�tier.Client;

public class ClientSQL implements ClientDAO {

	public ArrayList<Client> findAll() {
		ArrayList<Client> resAll=new ArrayList<Client>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Client");
			
			while (res.next()) {
				Client obj=new Client(res.getInt(1),res.getString(2), res.getString(3), res.getString(4), res.getString(5), res.getString(6), res.getString(7), res.getString(8));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Client : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	private static ClientSQL instance;
	private ClientSQL() {}
	public static ClientSQL getInstance() {
	if (instance==null) {
	instance = new ClientSQL();
	}
	return instance;
	}
	
	@Override
	public Client getById(int id) {
		Client c = new Client();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Client WHERE id_client = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			c = new Client(res.getInt(1), res.getString(2), res.getString(3), res.getString(4), res.getString(5),
					res.getString(6), res.getString(7), res.getString(8));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Client : create [ " + sqle.getMessage() + " ]");
		}
		return c;
	}

	@Override
	public void create(Client c) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Client VALUES(" + c.getIdClient() + ",'" + c.getNom() + "','" + c.getPrenom() + "','" + c.getRue()
					+ "','" + c.getVoie() + "','" + c.getCp() + "','" + c.getVille() + "','" + c.getPays() + "')");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Client c) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Client where id_client=" + c.getIdClient());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Client c) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Client SET nom='" + c.getNom() + "',prenom='" + c.getPrenom() + "',no_rue=" + c.getRue()
					+ ",voie='" + c.getVoie() + "',code_postal='" + c.getCp() + "',ville='" + c.getVille() + "',pays='" + c.getPays()
					+ "' where id_client=" + c.getIdClient() + "");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}
}
