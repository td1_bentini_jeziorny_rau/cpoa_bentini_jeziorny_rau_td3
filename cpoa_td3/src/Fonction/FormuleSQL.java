package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Connexion.Connexion;
import Dao.FormuleDAO;
import ObjetM�tier.Formule;

public class FormuleSQL implements FormuleDAO {
	
	public ArrayList<Formule> findAll() {
		ArrayList<Formule> resAll=new ArrayList<Formule>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Formule");
			
			while (res.next()) {
				Formule obj=new Formule(res.getInt(1),res.getInt(2));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Formule : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	private static FormuleSQL instance;
	private FormuleSQL() {}
	public static FormuleSQL getInstance() {
	if (instance==null) {
	instance = new FormuleSQL();
	}
	return instance;
	}
	
	@Override
	public Formule getById(int id) {
		Formule f = new Formule();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Formule WHERE id_revue = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			f = new Formule(res.getInt(1), res.getInt(2), res.getFloat(3));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Formule : create [ " + sqle.getMessage() + " ]");
		}
		return f;
	}

	@Override
	public void create(Formule f) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Formule VALUES(" + f.getIdRev() + "," + f.getIdDur() + ",'" + f.getReduc() + "')");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Formule f) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Formule where id_revue=" + f.getIdRev() + " AND id_duree=" + f.getIdDur());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Formule f) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Formule SET reduction='" + f.getReduc() + "' where id_revue=" + f.getIdRev()
					+ " AND id_duree=" + f.getIdDur());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}
	
}
