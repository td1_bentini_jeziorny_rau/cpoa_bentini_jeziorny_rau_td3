package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Connexion.Connexion;
import Dao.DureeDAO;
import ObjetM�tier.Duree;

public class DureeSQL implements DureeDAO{
	
	public ArrayList<Duree> findAll() {
		ArrayList<Duree> resAll=new ArrayList<Duree>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Duree");
			
			while (res.next()) {
				Duree obj=new Duree(res.getInt(1),res.getString(2));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Duree : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	private static DureeSQL instance;
	private DureeSQL() {}
	public static DureeSQL getInstance() {
	if (instance==null) {
	instance = new DureeSQL();
	}
	return instance;
	}
	
	@Override
	public Duree getById(int id) {
		Duree d = new Duree();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Duree WHERE id_duree = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			d = new Duree(res.getInt(1), res.getString(2));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Formule : create [ " + sqle.getMessage() + " ]");
		}
		return d;
	}

	@Override
	public void create(Duree d) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Duree VALUES(" + d.getId() + ",'" + d.getLibelle() + "')");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Duree d) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Duree where id_duree=" + d.getId());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Duree d) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Duree SET libelle_formule='" + d.getLibelle() + "' where id_duree=" + d.getId());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}
	
}
