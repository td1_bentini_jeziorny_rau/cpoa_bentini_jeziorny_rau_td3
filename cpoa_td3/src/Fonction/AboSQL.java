package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Connexion.Connexion;
import Dao.AbonnementDAO;
import ObjetM�tier.Abonnement;

public class AboSQL implements AbonnementDAO{
	
	
	private static AboSQL instance;
	private AboSQL() {}
	public static AboSQL getInstance() {
	if (instance==null) {
	instance = new AboSQL();
	}
	return instance;
	}
	
	public ArrayList<Abonnement> findAll() {
		ArrayList<Abonnement> resAll=new ArrayList<Abonnement>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Abonnement");
			
			while (res.next()) {
				Abonnement obj=new Abonnement(res.getInt(1),res.getInt(2), res.getString(3), res.getString(4), res.getInt(5));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Abonnement : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	
	@Override
	public Abonnement getById(int id) {
		Abonnement a = new Abonnement();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Abonnement WHERE id_client = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			a = new Abonnement(res.getInt(1), res.getInt(2), res.getString(3), res.getString(4), res.getInt(5));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Abonnement : create [ " + sqle.getMessage() + " ]");
		}
		return a;
	}

	@Override
	public void create(Abonnement a) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Abonnement VALUES(" + a.getIdClient() + "," + a.getIdRevue() + ",'" + a.getDateDeb() + "','" + a.getDateFin()
					+ "'," + a.getIdDur() + ")");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Abonnement a) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Abonnement where id_client=" + a.getIdClient() + " AND id_revue=" + a.getIdRevue());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Abonnement a) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Abonnement SET date_debut=CAST('" + a.getDateDeb() + "' AS DATE),date_fin=CAST('" + a.getDateFin()
					+ "' AS DATE),id_duree=" + a.getIdDur() + " WHERE id_client=" + a.getIdClient() + " AND id_revue=" + a.getIdRevue());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

}
