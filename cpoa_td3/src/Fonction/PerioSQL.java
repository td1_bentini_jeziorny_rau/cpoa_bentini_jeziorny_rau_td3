package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import Connexion.Connexion;
import Dao.PeriodiciteDAO;
import ObjetM�tier.Periodicite;

public class PerioSQL implements PeriodiciteDAO{
	
	private static PerioSQL instance;
	private PerioSQL() {}
	public static PerioSQL getInstance() {
	if (instance==null) {
	instance = new PerioSQL();
	}
	return instance;
	}
	
	public ArrayList<Periodicite> findAll() {
		ArrayList<Periodicite> resAll=new ArrayList<Periodicite>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Periodicite");
			
			while (res.next()) {
				Periodicite obj=new Periodicite(res.getInt(1),res.getString(2));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Periodicite : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	@Override
	public Periodicite getById(int id) {
		Periodicite p = new Periodicite();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Periodicite WHERE id_periodicite = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			p = new Periodicite(res.getInt(1), res.getString(2));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Periodicite : create [ " + sqle.getMessage() + " ]");
		}
		return p;
	}
	
	@Override
	public void create(Periodicite p) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Periodicite VALUES(" + p.getId() + ",'" + p.getLibelle() + "')");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Periodicite p) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Periodicite where id_periodicite=" + p.getId());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Periodicite p) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Periodicite SET libelle='" + p.getLibelle() + "' where id_periodicite=" + p.getId());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}
}
