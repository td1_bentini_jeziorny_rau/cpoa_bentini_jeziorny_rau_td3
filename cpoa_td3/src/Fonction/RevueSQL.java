package Fonction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import Connexion.Connexion;
import Dao.RevueDAO;
import ObjetM�tier.Revue;

public class RevueSQL implements RevueDAO{
	
	public ArrayList<Revue> findAll() {
		ArrayList<Revue> resAll=new ArrayList<Revue>();
		try{
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion(); 
			Statement requete=laConnexion.createStatement();
			ResultSet res=requete.executeQuery("SELECT * FROM Revue");
			
			while (res.next()) {
				Revue obj=new Revue(res.getInt(1),res.getString(2), res.getString(3), res.getInt(4), res.getString(5), res.getInt(6));
				resAll.add(obj);
			}
			res.close();
			laConnexion.close();	
		}
		catch (SQLException sqle){
			System.out.println("Revue : select all [ "+sqle.getMessage()+" ]");;
		}
		return resAll;
	}
	
	private static RevueSQL instance;
	private RevueSQL() {}
	public static RevueSQL getInstance() {
	if (instance==null) {
	instance = new RevueSQL();
	}
	return instance;
	}
	
	@Override
	public Revue getById(int id) {
		Revue r = new Revue();
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM Revue WHERE id_revue = ?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			r = new Revue(res.getInt(1), res.getString(2),res.getString(3),res.getFloat(4),res.getString(5),res.getInt(6));

			laConnexion.close();
		} catch (SQLException sqle) {
			System.out.println("Revue : create [ " + sqle.getMessage() + " ]");
		}
		return r;
	}

	@Override
	public void create(Revue r) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("INSERT INTO Revue VALUES(" + r.getIdRev() + ",'" + r.getTitre() + "','" + r.getDesc() + "'," + r.getTarif()
					+ ",'" + r.getVisuel() + "'," + r.getIdPerio() + ")");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void delete(Revue r) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("DELETE FROM Revue where id_revue=" + r.getIdRev());
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

	@Override
	public void update(Revue r) {
		try {
			Connexion co = new Connexion();
			Connection laConnexion = co.creeConnexion();
			Statement requete = laConnexion.createStatement();
			requete.executeUpdate("UPDATE Revue SET titre='" + r.getTitre() + "',description='" + r.getDesc() + "',tarif_num�ro="
					+ r.getTarif() + ",visuel='" + r.getVisuel() + "',id_periodicite=" + r.getIdPerio() + " where id_revue=" + r.getIdRev() + "");
			System.out.println("Requete effectu�e");
		} catch (SQLException sqle) {
			System.out.println("Pb select" + sqle.getMessage());
		}
	}

}
