package Dao;

import java.util.List;

import ObjetMétier.Client;

public interface ClientDAO extends DAO<Client>
{
	public abstract Client getById(int id);
	public abstract void create(Client objet);
	public abstract void update(Client objet);
	public abstract void delete(Client objet);
	List<Client> findAll();
}
