package Dao;

import java.util.List;

import ObjetMétier.Revue;

public interface RevueDAO extends DAO<Revue>
{
	public abstract Revue getById(int id);
	public abstract void create(Revue objet);
	public abstract void update(Revue objet);
	public abstract void delete(Revue objet);
	List<Revue> findAll();
}