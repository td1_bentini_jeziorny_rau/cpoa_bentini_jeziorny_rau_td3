package Dao;

import java.util.List;

import ObjetMétier.Periodicite;

public interface PeriodiciteDAO extends DAO<Periodicite>
{
	public abstract Periodicite getById(int id);
	public abstract void create(Periodicite objet);
	public abstract void update(Periodicite objet);
	public abstract void delete(Periodicite objet);
	List<Periodicite> findAll();
}
