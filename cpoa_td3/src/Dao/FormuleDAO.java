package Dao;

import java.util.List;

import ObjetMétier.Formule;

public interface FormuleDAO extends DAO<Formule>
{
	public abstract Formule getById(int id);
	public abstract void create(Formule objet);
	public abstract void update(Formule objet);
	public abstract void delete(Formule objet);
	List<Formule> findAll();
}
