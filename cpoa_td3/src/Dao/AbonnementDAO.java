package Dao;

import java.util.List;

import ObjetMétier.Abonnement;

public interface AbonnementDAO extends DAO<Abonnement>
{
	public abstract Abonnement getById(int id);
	public abstract void create(Abonnement objet);
	public abstract void update(Abonnement objet);
	public abstract void delete(Abonnement objet);
	List<Abonnement> findAll();
}
