package Dao;

import java.util.List;

import ObjetMétier.Duree;

public interface DureeDAO extends DAO<Duree>
{
	public abstract Duree getById(int id);
	public abstract void create(Duree objet);
	public abstract void update(Duree objet);
	public abstract void delete(Duree objet);
	List<Duree> findAll();
}
