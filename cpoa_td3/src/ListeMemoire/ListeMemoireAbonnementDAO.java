package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import Dao.AbonnementDAO;
import ObjetM�tier.Abonnement;

public class ListeMemoireAbonnementDAO implements AbonnementDAO{
        
        private static ListeMemoireAbonnementDAO instance;

        private List<Abonnement> donnees;


        public static ListeMemoireAbonnementDAO getInstance() {

                if (instance == null) {
                        instance = new ListeMemoireAbonnementDAO();
                }

                return instance;
        }

        private ListeMemoireAbonnementDAO() {

                this.donnees = new ArrayList<Abonnement>();

                this.donnees.add(new Abonnement(1,1,"2017-01-01","2017-10-01",1));

        }


        @Override
        public void create(Abonnement objet) {

                objet.setIdClient(3);

                // Ne fonctionne que si l'objet métier est bien fait...
                while (this.donnees.contains(objet)) {

                        objet.setIdClient(objet.getIdClient() + 1);
                }
        }

        @Override
        public void update(Abonnement objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
                } else {
                        this.donnees.set(idx, objet);
                }
        }

        @Override
        public void delete(Abonnement objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
                } else {
                        this.donnees.remove(idx);
                }
        }

        @Override
        public Abonnement getById(int id) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(new Abonnement(id, -1, "test", "test", -1));
                if (idx == -1) {
                        throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
                } else {
                        return this.donnees.get(idx);
                }
        }

        @Override
        public List<Abonnement> findAll() {

                return this.donnees;
        }

}
