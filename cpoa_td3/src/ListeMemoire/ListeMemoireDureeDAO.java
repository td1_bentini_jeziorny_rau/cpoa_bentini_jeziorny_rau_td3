package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import Dao.*;
import ObjetM�tier.*;

public class ListeMemoireDureeDAO implements DureeDAO {

	private static ListeMemoireDureeDAO instance;

	private List<Duree> donnees;


	public static ListeMemoireDureeDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoireDureeDAO();
		}

		return instance;
	}

	private ListeMemoireDureeDAO() {

		this.donnees = new ArrayList<Duree>();

		this.donnees.add(new Duree(1, "6 mois"));
	}


	@Override
	public void create(Duree objet) {

		objet.setId(3);

		// Ne fonctionne que si l'objet métier est bien fait...
		while (this.donnees.contains(objet)) {

			objet.setId(objet.getId() + 1);
		}
	}

	@Override
	public void update(Duree objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
		} else {
			this.donnees.set(idx, objet);
		}
	}

	@Override
	public void delete(Duree objet) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(objet);
		if (idx == -1) {
			throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
		} else {
			this.donnees.remove(idx);
		}
	}

	@Override
	public Duree getById(int id) {

		// Ne fonctionne que si l'objet métier est bien fait...
		int idx = this.donnees.indexOf(new Duree(id, "test"));
		if (idx == -1) {
			throw new IllegalArgumentException("Aucun objet ne poss�de cet identifiant");
		} else {
			return this.donnees.get(idx);
		}
	}

	@Override
	public List<Duree> findAll() {

		return this.donnees;
	}


}

