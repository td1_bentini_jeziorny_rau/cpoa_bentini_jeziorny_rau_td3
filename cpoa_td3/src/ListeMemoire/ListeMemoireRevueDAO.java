package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import Dao.RevueDAO;
import ObjetM�tier.Revue;

public class ListeMemoireRevueDAO implements RevueDAO {
        
        private static ListeMemoireRevueDAO instance;

        private List<Revue> donnees;


        public static ListeMemoireRevueDAO getInstance() {

                if (instance == null) {
                        instance = new ListeMemoireRevueDAO();
                }

                return instance;
        }

        private ListeMemoireRevueDAO() {

                this.donnees = new ArrayList<Revue>();

                this.donnees.add(new Revue(1, "Harry Potter", "Fantastique", 20, "Quelque chose", 1));
        }


        @Override
        public void create(Revue objet) {

                objet.setIdRev(3);

                // Ne fonctionne que si l'objet métier est bien fait...
                while (this.donnees.contains(objet)) {

                        objet.setIdRev(objet.getIdRev() + 1);
                }
        }

        @Override
        public void update(Revue objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
                } else {
                        this.donnees.set(idx, objet);
                }
        }

        @Override
        public void delete(Revue objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
                } else {
                        this.donnees.remove(idx);
                }
        }

        @Override
        public Revue getById(int id) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(new Revue(id, "test", "test", -1, "test", -1));
                if (idx == -1) {
                        throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
                } else {
                        return this.donnees.get(idx);
                }
        }

        @Override
        public List<Revue> findAll() {

                return this.donnees;
        }

}
