package ListeMemoire;

import java.util.ArrayList;
import java.util.List;

import Dao.FormuleDAO;
import ObjetM�tier.Formule;

public class ListeMemoireFormuleDAO implements FormuleDAO{
        
        private static ListeMemoireFormuleDAO instance;

        private List<Formule> donnees;


        public static ListeMemoireFormuleDAO getInstance() {

                if (instance == null) {
                        instance = new ListeMemoireFormuleDAO();
                }

                return instance;
        }

        private ListeMemoireFormuleDAO() {

                this.donnees = new ArrayList<Formule>();

                this.donnees.add(new Formule(1,1,5));

        }


        @Override
        public void create(Formule objet) {

                objet.setIdRev(3);

                // Ne fonctionne que si l'objet métier est bien fait...
                while (this.donnees.contains(objet)) {

                        objet.setIdRev(objet.getIdRev() + 1);
                }
        }

        @Override
        public void update(Formule objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de modification d'un objet inexistant");
                } else {
                        this.donnees.set(idx, objet);
                }
        }

        @Override
        public void delete(Formule objet) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(objet);
                if (idx == -1) {
                        throw new IllegalArgumentException("Tentative de suppression d'un objet inexistant");
                } else {
                        this.donnees.remove(idx);
                }
        }

        @Override
        public Formule getById(int id) {

                // Ne fonctionne que si l'objet métier est bien fait...
                int idx = this.donnees.indexOf(new Formule(id, -1, -1));
                if (idx == -1) {
                        throw new IllegalArgumentException("Aucun objet ne possède cet identifiant");
                } else {
                        return this.donnees.get(idx);
                }
        }

        @Override
        public List<Formule> findAll() {

                return this.donnees;
        }

}
