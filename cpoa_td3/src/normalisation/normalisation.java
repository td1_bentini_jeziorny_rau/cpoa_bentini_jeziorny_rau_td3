package normalisation;

import java.util.regex.Pattern;

public class normalisation {
	
	public static String Norm_pays(String nom) {

		if ((nom == "letzebuerg") || (nom == "Letzebuerg")) {
			nom = "Luxembourg";
		}

		else if (nom == "belgium" || nom == "Belgium") {
			nom = "Belgique";
		}

		else if (nom == "Switzerland" || nom == "Schweiz" || nom == "switzerland" || nom == "schweiz") {
			nom = "Suisse";
		}
		return nom;

	}

	public static String Norm_Voie(String voie) {

		String nvoie[] = voie.split(" ");
		if (!(Pattern.matches("^\\d, \n$", nvoie[0]))) {
			nvoie[0] = nvoie[0].concat(",");
		}

		if ((nvoie[1].startsWith("boul")) || (nvoie[1].startsWith("boul.")) || (nvoie[1].startsWith("bd"))) {
			nvoie[1] = "boulevard";
		}
		if (nvoie[1].startsWith("av.")){
			nvoie[1] = "avenue";
		}
		if ((nvoie[1].startsWith("faub.")) || (nvoie[1].startsWith("fg"))) {
			nvoie[1] = "faubourg";
		}
		if (nvoie[1].startsWith("pl.")) {
			nvoie[1] = "place";
		}
		System.out.println(nvoie[1]);

		voie = "";
		for (int i = 0; i <= nvoie.length - 1; i++) {
			if (i == 0)
				voie = nvoie[i];
			else
				voie = voie + ' ' + nvoie[i];
		}
		return voie;

	}

	public static String Norm_cp(String cp) {

		if (Pattern.matches("^\\d{4}$", cp)) {

			cp = "0".concat(cp);

		} else if (Pattern.matches("^L-\\d{4}$", cp)) {
			cp = cp.replace("L-", "");
		}
		return cp;
	}

	public static String Norm_ville(String ville) {

		ville = ville.trim();
		ville = ville.toLowerCase();
		String parties[] = ville.split(" ");

		for (int i = 0; i <= parties.length - 1; i++) {
			parties[i] = parties[i].substring(0, 1).toUpperCase() + parties[i].substring(1);
		}

		ville = " ";

		for (int i = 0; i <= parties.length - 1; i++) {
			if (i == 0)
				ville = parties[i];
			else
				ville = ville + "-" + parties[i];
		}

		if (ville.startsWith("St")) {
			ville = ville.replace("St", "Saint");
		}
		if (ville.startsWith("Ste")) {
			ville = ville.replace("Ste", "Sainte");
		}

		return ville;
	}

}
